<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->

## Systemadministraton 2 Übung
* SAM 2 Übung
* Devops

---
### CI/CD Pipelines with Gitlab
* Devops CI/CD Pipelines
* Version 20190521

---
## CI/CD Pipelines Übung Übersicht
* Voraussetzungen vLAB
* vLAB Environment
* vLAB Setup
* CI/CD Pipelines Einführung

---
## CI/CD Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 6 GB RAM
* Linux Ubuntu 16.04 LTS

---
## The vLAB Environment
* server.local
  * (Ubuntu 16.04 / Port 8082 / 1GB )
* runner.local
  * (Ubuntu 16.04 / Port 8084 / 2GB)
* gitlab.local
  * (Ubuntu 16.04 / Port 8003 / 4GB)


---
## Gitlab CI/CD Umgebung

![Gitlab CI/CD Umgebung](_images/cicd-environment.png)

---
## CI/CD Pipeline vLAB (Vagrant)
* git clone https://gitlab.com/rstumpner/sam-vlab-cicd-pipelines
* cd vlab/vagrant-virtualbox-ansible/
* vagrant up
* vagrant ssh runner
* Username: vagrant
* Password: vagrant

---
## CI/CD Pipeline vLAB (Vagrant)
* Edit Vagrantfile
* runner (1GB)
* server (1GB)
* vagrant up
* Resourcen Anpassen

---
## CI/CD Pipeline vLAB (Vagrant)
* vagrant up runner
* vagrant up server
* Alternative für gitlab.local (https://www.gitlab.com)

---
## Check Gitlab Installation
* Browser: http://localhost:8083
* User: root
* Password: sam123456

---
## Gitlab User erstellen
* Create User

![Gitlab User erstellen](_images/gitlab-create-user.png)

---
## Gitlab Projekt erstellen
* Create Project
![Gitlab User erstellen](_images/gitlab-create-projekt.png)

---
## Gitlab Projekt

![Gitlab User erstellen](_images/gitlab-projekt-view.png)

---
## Arbeiten mit Gitlab

* git clone http://gilab.local/sam-gitlab-playground
* cd sam-gitlab-playground
* echo "mychange" >> readme.md
* git status
* git add .
* git commit -a
* git push

---
## Editor
* ATOM (https://atom.io/)
* Visualstudio Code (https://code.visualstudio.com/)

---
## First CI/CD Pipeline
* .gitlab-ci.yml

```YAML
stages:
  - testpipe

variables:
  ANSIBLE_FORCE_COLOR: 'true'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_HOST_KEY_CHECKING: 'false'

pipeline-test:
  stage: testpipe
  image: ubuntu:latest
  script:
    - '/bin/echo \"CICD Pipeline Tested\"'
```

---
## Check gitlab Runner

```bash
ssh vagrant runner
ping gitlab.local
ping server.local
```

---
## Gitlab-Runner Installation
* Get a Gitlab Runner Token from Gitlab

---
## Gitlab Runner Token
![Gitlab Runner Token](_images/gitlab-runner-token.png)


---
## Gitlab-Runner Installation

* vagrant ssh runner
* Register Runner on Gitlab:
  * sudo gitlab-runner register --name my-runner --url http://gitlab.local --registration-token $TOKEN
* Run Gitlab
  * gitlab-runner run

---
## Gitlab Executor
* Docker
* SSH

---
## Check Gitlab Runner

```TOML
# /etc/gitlab-runner/config.toml
concurrent = 1
check_interval = 0
[[runners]]
  name = "my-runner"
  url = "http://172.28.128.8"
  token = "9fcdd17f3bc6f4128638266383c234"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ubuntu:latest"
    privileged = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    dns           = ["8.8.8.8"]
    extra_hosts = ["gitlab.local:172.28.128.8","server.local:172.28.128.3"]
  [runners.cache]
```

---
## Gitlab CI/CD and Ansible
* Create a new Project: poc-pipeline

---
## First CI/CD Pipeline
* inventory/inventory.yml
```YAML
all:
  hosts:
    server:
      ansible_ssh_host: server.local
      ansible_ssh_user: ansible
      ansible_ssh_password: ansible
      ansible_host: server.local
      ansible_user: ansible
      ansible_password: ansible
      ansible_sudo_pass: ansile
```

---
## First CI/CD Pipeline
* first-playbook.yml

```YAML
- hosts: server
#  become: yes

  tasks:
   - name: Clone Kanboard source from deploy server
     git:
      repo: 'https://github.com/kanboard/kanboard.git'
      dest: '/tmp/kanboard'
      version: 'master'
      update: False
```

---
## First CI/CD Pipeline mit Ansible
* .gitlab-ci.yml

```YAML
stages:
  - testpipe
  - lint
  - testing

variables:
  ANSIBLE_FORCE_COLOR: 'true'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_HOST_KEY_CHECKING: 'false'

pipeline-test:
  stage: testpipe
  image: ubuntu:latest
  script:
    - '/bin/echo \"CICD Pipeline Tested\"'

ansible-syntax:
  stage: lint
  image: ubuntu:latest
  before_script:
    - apt-get update -qq
    - apt install --no-install-recommends -y python-minimal software-properties-common git vim iputils-ping mtr dnsutils rsync tree python-pip
    - apt-add-repository ppa:ansible/ansible
    - apt update
    - apt install -y ansible python-markupsafe python-ecdsa libyaml-0-2 python-jinja2 python-yaml python-paramiko python-httplib2 python-crypto sshpass
  script:
    - 'ansible-playbook -i inventory/inventory.yml playbooks/first.yml --syntax-check'

```

---
## Aufgabe
Fasse die folgenden Aufgaben in ein Playbook und führe diese auf dem System server.local über die CI/CD Pipeline aus

---
## Aufgabe
Ändere die Message of the Day

---
## Aufgabe
Installiere einen Apache Webserver

---
## Aufgabe
Installiere die neueste mit dem Ubuntu Paketsystem verfügbare PHP Version

---
## Aufgabe
Überprüfe die Apache und PHP Installation

---
## Aufgabe
Installiere die PHP Webapplikation Kanboard (https://kanboard.org)

---
## Fragen

---
## Notizen

---
## Ansible Inventory
![Ansible Inventory](_images/ansible-inventory.png)
